import sys
from logging import getLogger
from logging import FileHandler, StreamHandler, DEBUG, INFO, Formatter
from src.config import log_file


def get_logger(is_debug=False):
    """ Возвращает логгер """
    log = getLogger("check_palindrome_service")
    formatter = Formatter('%(levelname)s: %(asctime)s: %(message)s')
    file_handler = FileHandler(log_file)
    file_handler.setFormatter(formatter)
    log.addHandler(file_handler)
    stream_handler = StreamHandler(stream=sys.stdout)
    stream_handler.setFormatter(formatter)
    log.addHandler(stream_handler)
    log.setLevel(DEBUG if is_debug else INFO)
    return log
