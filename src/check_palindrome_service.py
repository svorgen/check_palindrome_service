from src.database import Database
from src.logger import get_logger


class Service:
    def __init__(self):
        self.database = Database()
        self.log = get_logger()

    async def check_palindrome(self, string: str) -> bool:
        """ Проверяет является ли строка палиндромом """
        result = string == string[::-1]
        self.log.info(f'Строка "{string}" {"является" if result else "не является"} палиндромом')
        await self.database.save_request(string, result)
        return result

    async def get_requests(self, offset: int, limit: int):
        """ Получает набор запросов """
        return await self.database.get_requests(offset, limit)

    async def delete_request(self, id: int):
        """ Удаляет запрос """
        return await self.database.delete_request(id)

    async def update_request(self, id: int, result: bool):
        """ Изменяет результат запроса """
        return await self.database.update_request(id, result)


service = Service()
