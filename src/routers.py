from fastapi import APIRouter
from pydantic import BaseModel
from src.check_palindrome_service import service


router = APIRouter()


class CheckPalindrome(BaseModel):
    string: str


class Delete(BaseModel):
    id: int


class Update(BaseModel):
    id: int
    result: bool


@router.post('/')
async def check_palindrome(request: CheckPalindrome) -> bool:
    return await service.check_palindrome(request.string)


@router.get('/')
async def get_requests(offset: int, limit: int):
    return await service.get_requests(offset, limit)


@router.post('/delete')
async def delete_request(request: Delete):
    return await service.delete_request(request.id)


@router.post('/update')
async def update_request(request: Update):
    return await service.update_request(request.id, request.result)
