from time import time
from src.models import RequestModel


class Database:
    async def save_request(self, string: str, result: bool) -> int:
        """ Сохраняет запрос в базу данных """
        return (
            await RequestModel(
                string=string,
                result=result,
                timestamp=time()
            ).create()
        ).id

    async def get_requests(self, offset: int, limit: int):
        """ Возвращает набор запросов из базы данных """
        return [request.__values__ for request in await RequestModel.get_requests(offset, limit)]

    async def delete_request(self, id: int):
        """ Удаляет запрос из базы данных """
        return await RequestModel.delete_request(id)

    async def update_request(self, id: int, result: bool):
        """ Обновляет результат запроса в базе данных """
        return await RequestModel.update_result(id, result)
