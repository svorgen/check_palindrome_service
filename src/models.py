from gino import Gino
from sqlalchemy import Column, BigInteger, Boolean, String, Float


db = Gino()


async def init_db(database: str, dsn: str):
    """ Присоединяет gino к циклу событий """
    await db.set_bind(f"{database}://{dsn}")


class RequestModel(db.Model):
    __tablename__ = 'request'
    id = Column(BigInteger, primary_key=True)
    string = Column(String)
    result = Column(Boolean)
    timestamp = Column(Float)

    @classmethod
    async def update_result(cls, id: int, result: bool):
        await cls.update.values(result=result).where(cls.id == id).gino.status()

    @classmethod
    async def delete_request(cls, id: int):
        await cls.delete.where(cls.id == id).gino.status()

    @classmethod
    async def get_requests(cls, offset: int, limit: int):
        return await cls.query.offset(offset).limit(limit).order_by(cls.id).gino.all()
